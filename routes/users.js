var express = require('express'),
    users = require(appRoot + '/app/controllers/users.js');

var router = express.Router();


/* GET users listing. */
router.get('/', users.index);

router.get('/register', users.registerGet);

router.post('/register', users.registerPost);

router.get('/login', users.loginGet);

router.post('/login', users.loginPost);

router.post('/logout', users.logout);

module.exports = router;
