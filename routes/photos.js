var express = require('express'),
    photos = require(appRoot + '/app/controllers/photos.js');

var router = express.Router();

router.get('/', photos.index);

router.post('/', photos.add);

router.get('/view/:id', photos.view);

router.get('/:id', photos.get);

router.put('/:id', photos.update);

router.delete('/:id', photos.delete);

module.exports = router;
