var mongoose = require('mongoose'),
    uniqueValidator = require('mongoose-unique-validator'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

var eventEmitter = require(appRoot + '/lib/event-emitter.js');

var userSchema = new Schema({
    name: String,
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    profile_pic: String,
});

userSchema.plugin(uniqueValidator);

userSchema.pre('save', function(next) {
    var user = this;

    if (!user.isModified('password')) return next();

    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

var User = mongoose.model('User', userSchema);

User.login = function(sess, username, password, login){

    User.findOne({ username: username }, function(err, user) {

        if( user ){
            user.comparePassword(password, function(err, isMatch) {

                sess.userId = user._id;

                eventEmitter.emit('userLoggedIn', user._id);

                login(err, isMatch);

            });
        } else {
            login(err, false);
        }

    });

};

User.logout = function(sess){

    delete sess.userId;

    eventEmitter.emit('userLoggedOut');

};

User.isLoggedIn = function(sess, loggedIn){

    if( sess.userId ){

        loggedIn( sess.userId );

    } else {

        loggedIn( false );

    }

};

module.exports = User;