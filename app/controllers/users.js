var eventEmitter = require(appRoot + '/lib/event-emitter.js');

var User = require(appRoot + '/app/models/user');

var users = {

    index: function(req, res, next) {

        User.find({}, function(err, users) {
            if (err) throw err;

            var response = {};
            response.users = {};

            for( var i in users ){
                response.users[i] = {
                    id: users[i]._id,
                    name: users[i].name,
                    username: users[i].username
                };
            }

            res.json(response);
        });

    },

    registerGet: function(req, res, next) {
        res.send('Here must be registration form');
    },

    registerPost: function(req, res, next) {

        var newUser = User({
            name: req.body.name,
            username: req.body.username,
            password: req.body.password,
        });

        newUser.save(function(errors) {

            var response = {};

            if( !errors ){

                response.success = 1;

                eventEmitter.emit('userRegistered');

            } else {

                response.success = 0;
                response.errors = {};

                for( var field in errors.errors ){
                    response.errors[ field ] = errors.errors[field].message;
                }

            }

            res.json(response);

        });

    },

    loginGet: function(req, res, next) {
        res.send('Here must be login form');
    },

    loginPost: function(req, res, next) {

        var login = function(err, isMatch){

            var response = {};

            response.success = isMatch ? 1 : 0;

            res.json(response);

        };

        User.login(req.session, req.body.username, req.body.password, login);

    },

    logout: function(req, res, next) {

        User.logout( req.session );

        res.json({success: 1});

    }

};

module.exports = users;