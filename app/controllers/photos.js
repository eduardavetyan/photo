var eventEmitter = require(appRoot + '/lib/event-emitter.js');

var User = require(appRoot + '/app/models/user');
var Photo = require(appRoot + '/app/models/photo');

var photos = {

    index: function(req, res, next) {

        var loggedIn = function( userId ){

            if( userId ){

                var foundPhotos = function(err, photos){

                    if (err) throw err;

                    var response = {};

                    response.success = 1;

                    response.photos = photos;

                    res.json(response);

                };

                Photo.findUserPhotos(userId, foundPhotos);

            } else {

                var response = {};

                response.success = 0;
                response.error = "The user is not authorized.";

                res.json(response);

            }

        };

        User.isLoggedIn(req.session, loggedIn);

    },

    add: function(req, res, next) {

        var loggedIn = function( userId ){

            if( userId ){

                if( req.files.photo ){

                    var photo = req.files.photo;

                    var filePath = appRoot + '/upload/'+Math.random().toString(36).substring(7)+'.jpg';

                    photo.mv(filePath, function(err) {
                        if (err)
                            return res.status(500).send(err);

                        var newPhoto = Photo({
                            title: req.body.title,
                            tags: req.body.tags.split(","),
                            private: req.body.private,
                            user_id: userId,
                            coordinates: req.body.coordinates.split(","),
                            path: filePath,
                        });

                        newPhoto.save(function(errors) {

                            if( !errors ){

                                eventEmitter.emit('photoUploaded', newPhoto._id);

                            } else {

                                response.errors = {};

                                for( var field in errors.errors ){
                                    response.errors[ field ] = errors.errors[field].message;
                                }

                            }

                        });

                        var response = {};

                        response.success = 1;

                        res.json(response);

                    });

                } else {

                    var response = {};

                    response.success = 0;
                    response.error = "File must be uploaded.";

                    res.json(response);

                }

            } else {

                var response = {};

                response.success = 0;
                response.error = "The user is not authorized.";

                res.json(response);

            }

        };

        User.isLoggedIn(req.session, loggedIn);

    },

    view: function(req, res, next) {

        var loggedIn = function( userId ){

            if( userId ){

                Photo.findOne({ _id: req.params.id }, function(err, photo) {

                    if( photo ){

                        if( photo.private == false || ( photo.private == true && photo.user_id == userId ) ){

                            res.sendFile(photo.path);

                        } else {

                            next();

                        }

                    } else {

                        next();

                    }

                });

            } else {

                var response = {};

                response.success = 0;
                response.error = "The user is not authorized.";

                res.json(response);

            }

        };

        User.isLoggedIn(req.session, loggedIn);

    },

    update: function(req, res, next) {

        var loggedIn = function( userId ){

            if( userId ){



                var getUpdateData = function(updateDataFunc){

                    var updateData = {
                        title: req.body.title,
                        tags: req.body.tags.split(","),
                        private: req.body.private,
                        coordinates: req.body.coordinates.split(",")
                    };

                    if( req.files.photo ){

                        var photo = req.files.photo;

                        var filePath = appRoot + '/upload/'+Math.random().toString(36).substring(7)+'.jpg';

                        photo.mv(filePath, function(err) {
                            if (err)
                                return res.status(500).send(err);

                            updateData.path = filePath;

                            updateDataFunc( updateData );

                        });

                    } else {

                        updateDataFunc( updateData );

                    }

                };

                var updateDataFunc = function(updateData){

                    Photo.findOneAndUpdate({_id: req.params.id, user_id: userId}, updateData, function(err, photo) {

                        if (err){
                            next( err );
                            return false;
                        };

                        if( photo ){

                            eventEmitter.emit('photoUpdated', photo._id);

                            var response = {};

                            response.success = 1;

                            res.json(response);

                        } else {

                            next();

                        }
                    });

                };

                getUpdateData( updateDataFunc );

            } else {

                var response = {};

                response.success = 0;
                response.error = "The user is not authorized.";

                res.json(response);

            }

        };

        User.isLoggedIn(req.session, loggedIn);

    },

    delete: function(req, res, next) {

        var loggedIn = function( userId ){

            if( userId ){

                Photo.findOneAndRemove({_id: req.params.id, user_id: userId}, function(err, photo) {

                    if (err){
                        next( err );
                        return false;
                    };

                    if( photo ){

                        eventEmitter.emit('photoDeleted');

                        var response = {};

                        response.success = 1;

                        res.json(response);

                    } else {

                        next();

                    }
                });

            } else {

                var response = {};

                response.success = 0;
                response.error = "The user is not authorized.";

                res.json(response);

            }

        };

        User.isLoggedIn(req.session, loggedIn);

    },

    get: function(req, res, next) {

        var loggedIn = function( userId ){

            if( userId ){

                Photo.findOne({ _id: req.params.id }, function(err, photo) {

                    if( photo ){

                        if( photo.private == false || ( photo.private == true && photo.user_id == userId ) ){

                            var response = {};

                            response.success = 1;
                            response.photo = photo;

                            res.json(response);

                        } else {

                            next();

                        }

                    } else {

                        next();

                    }

                });

            } else {

                var response = {};

                response.success = 0;
                response.error = "The user is not authorized.";

                res.json(response);

            }

        };

        User.isLoggedIn(req.session, loggedIn);

    }

};

module.exports = photos;